<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package MMA
 */

$imagem_evento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0];

$random_posts = new WP_Query( array('post_type' => 'evento', 'orderby' => 'rand', 'posts_per_page' => '3') );

get_header();
?>

<main class="pg pg-evento">
	<section class="secao-destaque-single" style="background-image: url( <?php echo $imagem_evento; ?> );">
		<h4 class="hidden">SEÇÃO DESTAQUE EVENTO</h4>
		<figure class="hidden">
			<img src="<?php echo $imagem_evento; ?>" alt="<?php echo get_the_title(); ?>">
			<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
		</figure>
	</section>

	<section class="secao-conteudo-evento">
		<h4 class="hidden">SEÇÃO CONTEÚDO EVENTO</h4>
		<div class="small-container">
			<a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="voltar"><img src="<?= get_template_directory_uri(); ?>/img/arrowservicosleftblack.svg" alt="Seta voltar"></a>
			<article>
				<h1 class="titulo"><?php echo get_the_title(); ?></h1>
				<span class="data-evento"><?php echo get_the_date( 'j M Y' ); ?></span>
				<?php while(have_posts()){ the_post(); echo the_content(); } ?>
			</article>
		</div>
	</section>

	<section class="secao-eventos-relacionados">
		<h4 class="hidden">SEÇÃO EVENTOS RELACIONADOS</h4>
		<div class="large-container">
			<h2 class="titulo">Eventos relacionados</h2>
			<ul class="lista-eventos">

				<?php while($random_posts->have_posts()): $random_posts->the_post(); 
					$imagem_evento_relacionado = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0]; ?>
				<li>
					<a href="<?php echo get_permalink(); ?>" class="link-imagem">
						<figure>
							<img src="<?php echo $imagem_evento_relacionado; ?>" alt="<?php echo get_the_title(); ?>">
							<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
						</figure>
					</a>
					<span class="data-evento"><?php echo get_the_date( 'j M Y' ); ?></span>
					<a href="<?php echo get_permalink(); ?>" class="link-titulo">
						<h2 class="titulo titulo-evento"><?php echo get_the_title(); ?></h2>
					</a>
				</li>
				<?php endwhile; ?>

			</ul>
		</div>
	</section>
	
	<div class="pop-up-galeria-eventos">
		<span>
			<img src="<?php echo get_template_directory_uri(); ?>/img/close.svg" alt="Fechar pop-up">
		</span>
		<ul class="carrossel-galeria-eventos owl-carousel">
			
		</ul>
	</div>
	
	<!-- DISQUS -->
	<div class="disqus">
		<?php 
		// If comments are open or we have at least one comment, load up the comment template.
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif; 
		?>
	</div>

	<?php  

		include (TEMPLATEPATH . '/inc/mma_agendamento.php');
		include (TEMPLATEPATH . '/inc/acessoria_mma.php');
		include (TEMPLATEPATH . '/inc/mma_localizacao.php');

		include (TEMPLATEPATH . '/inc/pop-up-agendamento.php');
	?>
	
</main>

<?php get_footer();
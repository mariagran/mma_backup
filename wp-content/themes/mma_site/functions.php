<?php
/**
 * MMA functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package MMA
 */

if ( ! function_exists( 'mma_site_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function mma_site_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on MMA, use a find and replace
		 * to change 'mma_site' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'mma_site', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'mma_site' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'mma_site_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'mma_site_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function mma_site_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'mma_site_content_width', 640 );
}
add_action( 'after_setup_theme', 'mma_site_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mma_site_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'mma_site' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'mma_site' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'mma_site_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function mma_site_scripts() {

	//CSS
	wp_enqueue_style( 'mma_animate_css', get_template_directory_uri() . '/css/animate.css');
	wp_enqueue_style( 'mma_carousel', get_template_directory_uri() . '/owlcarousel/dist/assets/owl.carousel.min.css');
	wp_enqueue_style( 'mma_owl_theme_default', get_template_directory_uri() . '/owlcarousel/dist/assets/owl.theme.default.min.css');
	wp_enqueue_style( 'mma_bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style( 'mma_style', get_stylesheet_uri() );

	//JAVA SCRIPT
	wp_enqueue_script( 'gran_jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js' );
	wp_enqueue_script( 'mma_jquery_mask', get_template_directory_uri() . '/js/jquery-mask/dist/jquery.mask.min.js' );
	wp_enqueue_script( 'mma_owlcarousel', get_template_directory_uri() . '/owlcarousel/dist/owl.carousel.min.js' );
	wp_enqueue_script( 'mma_bootstrapl', get_template_directory_uri() . '/js/bootstrap.min.js' );
	wp_enqueue_script( 'mma_geral', get_template_directory_uri() . '/js/geral.js' );

	

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'mma_site_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * módulos MMA.
 */
require get_template_directory() . '/inc/modulos.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

//REDUX FRAMEWORK
if (class_exists('ReduxFramework')) {
	require_once (get_template_directory() . '/redux/sample-config.php');
}

//REGISTRAR SVG
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// VERSIONAMENTO DE FOLHAS DE ESTILO
function my_wp_default_styles($styles){
	$styles->default_version = "18052020";
}
add_action("wp_default_styles", "my_wp_default_styles");

	// VERSIONAMENTO DE FOLHAS DE ESTILO
function my_wp_default_scripts($scripts){
	$scripts->default_version = "18052020";
}
add_action("wp_default_scripts", "my_wp_default_scripts");

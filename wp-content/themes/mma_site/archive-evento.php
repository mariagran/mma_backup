<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MMA
 */

get_header();

?>

<main class="pg pg-eventos">
	<section class="secao-destaque-paginas">
		<h4 class="hidden">SEÇÃO DESTAQUE PÁGINAS</h4>
		<div class="large-container">
			<article>
				<h1 class="titulo">Eventos</h1>
				<p>Aqui no MMA Lavagens Especiais você encontra tudo para a estética do seu carro, desde uma lavagem simples, até a melhor proteção de pintura presente hoje no mercado mundial.</p>
			</article>
		</div>
	</section>

	<section class="secao-eventos">
		<h4 class="hidden">SEÇÃO EVENTOS</h4>
		<div class="large-container">
			<ul class="lista-eventos">

				<?php while(have_posts()): the_post();
						$imagem_evento = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0]; ?>
				<li>
					<a href="<?php echo get_permalink(); ?>" class="link-imagem">
						<figure>
							<img src="<?php echo $imagem_evento; ?>" alt="<?php echo get_the_title(); ?>">
							<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>
						</figure>
					</a>
					<span class="data-evento"><?php echo get_the_date( 'j M Y' ); ?></span>
					<a href="<?php echo get_permalink(); ?>" class="link-titulo">
						<h2 class="titulo titulo-evento"><?php echo get_the_title(); ?></h2>
					</a>
				</li>
				<?php endwhile; ?>

			</ul>
		</div>
	</section>

	<?php  

		include (TEMPLATEPATH . '/inc/mma_agendamento.php');
		include (TEMPLATEPATH . '/inc/acessoria_mma.php');
		include (TEMPLATEPATH . '/inc/mma_localizacao.php');

		include (TEMPLATEPATH . '/inc/pop-up-agendamento.php');
	?>

</main>

<?php get_footer();
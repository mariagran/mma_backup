<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package MMA
 */

global $post;

$numero_whtas_remocao_espaco_externo = trim($configuracao['opt_telefone']);
$numero_whtas_remocao_traco = str_replace(" ","",$numero_whtas_remocao_espaco_externo);
$numeroFomratado = str_replace("-","",$numero_whtas_remocao_traco);

$categoria = 'categoriaservicos';

$categoriaAtual = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );

$categoriaAtualLoop1 = get_terms( $categoria, array(
	'orderby'    => 'count',
	'hide_empty' => true,
	'parent'	 => '',
	'order'      => 'DESC',
));

$categoriaAtualLoop2 = get_terms( $categoria, array(
	'orderby'    => 'count',
	'hide_empty' => true,
	'parent'	 => '',
	'order'      => 'DESC',
));

$categoriaAtualLoop3 = get_terms( $categoria, array(
	'orderby'    => 'count',
	'hide_empty' => true,
	'parent'	 => '',
	'order'      => 'DESC',
));

// var_dump($categoriaAtualLoop);
// var_dump($categoriaAtual);

// $projeto_ferramentas = rwmb_meta('MMA_servico_projeto');

get_header(); ?>

<style type="text/css">
	.pg-servicos section.servico-destaque{
		background-image: url(<?php echo $configuracao['opt_texto_servicos_banner']['url'] ?>)
	}
	@media(max-width: 500px){
		.pg-servicos section.servico-destaque{
			background-image: url(<?php echo $configuracao['opt_texto_servicos_banner_mobile']['url'] ?>)
		}
	}
</style>

<main class="pg pg-servicos">

	<section class="servico-destaque" style="background-image: url(<?php echo $configuracao['opt_texto_servicos_banner']['url'] ?>)">
		
		<div class="servico-destaque-texto large-container">
			<h3 class="titulo"><?php echo $configuracao['opt_titulo_servicos'] ?></h3>
			<p><?php echo $configuracao['opt_texto_servicos'] ?></p>
		</div>

		<figure>
			<img src="<?php echo $configuracao['opt_texto_servicos_banner']['url'] ?>" alt="<?php echo $configuracao['opt_titulo_servicos'] ?>">
			<figcaption class="hidden"><?php echo $configuracao['opt_titulo_servicos'] ?></figcaption>
		</figure>

		<nav class="servico-destaque-menu large-container">
			<?php 
				foreach ($categoriaAtualLoop3 as $categoriaAtualLoop3):
					$categoriaAtualName    = $categoriaAtualLoop3->name;
					$categoriaAtualTerm_id = $categoriaAtualLoop3->term_id;
					if($categoriaAtualLoop3->slug === $categoriaAtual->slug):
			?>
			<div class="categoria-ativa">
				<a href="#">
					<figure class="categoria-ativa-imagem">
						<img src="<?php echo z_taxonomy_image_url($categoriaAtualTerm_id); ?>" alt="">
					</figure>
					<h3 class="categoria-ativa-titulo titulo"><?php echo $categoriaAtualName; ?></h3>
				</a>
			</div>
			<?php endif; endforeach; ?>
			<hr>
			<ul>
				<?php 
					$i = 0; 
					foreach ($categoriaAtualLoop1 as $categoriaAtualLoop1):
						$categoriaAtualName    = $categoriaAtualLoop1->name;
						$categoriaAtualTerm_id = $categoriaAtualLoop1->term_id;
						$classeCss = ($categoriaAtualLoop1->slug === $categoriaAtual->slug) ? "ativo" : "";
				?>
				<li data-id="<?php echo $categoriaAtualTerm_id ?>" data-link="<?php echo get_term_link($categoriaAtualTerm_id); ?>" data-icone="<?php echo z_taxonomy_image_url($categoriaAtualTerm_id); ?>" data-titulo="<?php echo $categoriaAtualName ?>" class="<?php echo $classeCss; ?>">
					<a href="<?php echo get_term_link($categoriaAtualTerm_id); ?> ">
						<figure>
							<img src="<?php echo z_taxonomy_image_url($categoriaAtualTerm_id); ?>" alt="<?php echo $categoriaAtualName; ?>">
						</figure>
						<h3><?php echo $categoriaAtualName ?></h3>
					</a>
				</li>
				<?php $i++; endforeach; ?>
			</ul>
		</nav>

	</section>

	<div id="servico-ancora"></div>

	<?php
		$cont = 0; 
		foreach ($categoriaAtualLoop2 as $categoriaAtualLoop2):
			$classeCss = ($categoriaAtualLoop2->slug === $categoriaAtual->slug) ? "ativo" : "";
	?>
	<div id="<?php echo $categoriaAtualLoop2Term_id = $categoriaAtualLoop2->term_id; ?>" class="areaServicos <?php echo $classeCss; ?>">
		
		<?php 
			$servicos = new WP_Query(array(
				'post_type'     => 'servico',
				'posts_per_page'   => -1,
				'tax_query'     => array(
					array(
						'taxonomy' => 'categoriaservicos',
						'field'    => 'slug',
						'terms'    => $categoriaAtualLoop2->slug,
						)
					)
				)
			);

			if ($servicos):

				// LOOP DE POST
				$valor = 0;
				while ( $servicos->have_posts() ) : $servicos->the_post();
					$servicosFoto = wp_get_attachment_image_src( get_post_thumbnail_id($servicos->ID), 'full' )[0];
					$servicoFotoMobile = rwmb_meta('MMA_banner_destaque_servicos_mobile')['full_url'];
					$projeto_ferramentas = rwmb_meta('MMA_servico_projeto');
					if($valor % 2 == 0):
					    
		?>
		<section class="servico-lista">
			
			<div class="row">
				
				<div class="col-sm-6">
					<div class="servico-lista-foto">
						<figure class="figure-desktop">
							<img src="<?php echo $servicosFoto; ?>" alt="<?php echo get_the_title() ?>">
							<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
						</figure>
						<figure class="figure-mobile">
							<img src="<?php echo $servicoFotoMobile; ?>" alt="<?php echo get_the_title() ?>">
							<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
						</figure>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="servico-lista-texto">
						
						<article>
							<h2><?php echo get_the_title() ?></h2>
							<p><?php echo get_the_content() ?></p>
							<ul>
								<?php 
									$servico_list = rwmb_meta('MMA_servico_list');
									foreach ($servico_list  as $servico_list ):
										$servico_list = $servico_list;
										
								 ?>
								<li><p><?php echo $servico_list ?></p></li>
								<?php endforeach; ?>
							</ul>
						</article>
						
						<div class="servico-lista-projetos">
							<h4>Projetos que receberam esse tratamento</h4>
							<ul>
								<?php 
									$contador_servicos2 = 0;
									//LOOP DE POST PROJETOS
									$projetos = new WP_Query( array( 'post_type' => 'projeto', 'orderby' => 'id', 'posts_per_page' => -1) );
									while ( $projetos->have_posts() ) : $projetos->the_post();
										// var_dump($projetos->ID);
										$verificacao =  in_array((string)$post->ID,$projeto_ferramentas);
										if ($verificacao):
										// var_dump((string)$post->ID);
								?>
								<li>
									<a href="<?php echo get_permalink() ?>">
										<figure>
											<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
											<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
										</figure>
									</a>
								</li>
								<?php endif; endwhile; wp_reset_query(); ?>
							</ul>
						</div>

						<div class="servico-lista-contato">
							<div class="div-button-padrao">
								<span class="button-padrao button-padrao-preto">Agendar horário</span>
							</div>
							<p class="whatsapp">
									<a href="https://api.whatsapp.com/send?phone=+55<?php echo $numeroFomratado; ?>&text=Ol%C3%A1!"><?php echo $configuracao['opt_telefone']; ?></a>
							</p>
						</div>

					</div>
				</div>

			</div>

		</section>
		<?php else: ?>
		<section class="servico-lista">
			
			<div class="row">
				
				<div class="col-md-6 col-mobile">
					<div class="servico-lista-foto">
						<figure>
							<img src="<?php echo $servicoFotoMobile; ?>" alt="<?php echo get_the_title() ?>">
							<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
						</figure>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="servico-lista-texto">
						
						<article>
							<h2><?php echo get_the_title() ?></h2>
							<p><?php echo get_the_content() ?></p>
							<ul>
								<?php 
									$servico_list = rwmb_meta('MMA_servico_list');
									foreach ($servico_list  as $servico_list ):
										$servico_list = $servico_list;
										
								 ?>
								<li><p><?php echo $servico_list ?></p></li>
								<?php endforeach; ?>
							</ul>
						</article>
						
						<div class="servico-lista-projetos">
							<h4>Projetos que receberam esse tratamento</h4>
							<ul>
								<?php 
									$contador_servicos2 = 0;
									//LOOP DE POST PROJETOS
									$projetos = new WP_Query( array( 'post_type' => 'projeto', 'orderby' => 'id', 'posts_per_page' => -1) );
									while ( $projetos->have_posts() ) : $projetos->the_post();
										// var_dump($projetos->ID);
										$verificacao =  in_array((string)$post->ID,$projeto_ferramentas);
										if ($verificacao):
										// var_dump((string)$post->ID);
								?>
								<li>
									<a href="<?php echo get_permalink() ?>">
										<figure>
											<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
											<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
										</figure>
									</a>
								</li>
								<?php endif; endwhile; wp_reset_query(); ?>
							</ul>
						</div>

						<div class="servico-lista-contato">
							<div class="div-button-padrao">
								<span class="button-padrao button-padrao-preto">Agendar horário</span>
							</div>
							<p class="whatsapp">
									<a href="https://api.whatsapp.com/send?phone=+55<?php echo $numeroFomratado; ?>&text=Ol%C3%A1!"><?php echo $configuracao['opt_telefone']; ?></a>
							</p>
						</div>

					</div>
				</div>

				<div class="col-md-6 col-desktop">
					<div class="servico-lista-foto">
						<figure>
							<img src="<?php echo $servicosFoto; ?>" alt="<?php echo get_the_title() ?>">
							<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
						</figure>
					</div>
				</div>

			</div>

		</section>
		<?php endif; $valor++; endwhile; endif; wp_reset_query(); ?>

	</div>
	<?php $cont++; endforeach; ?>
	
	<?php  
		include (TEMPLATEPATH . '/inc/mma_store.php');
		include (TEMPLATEPATH . '/inc/acessoria_mma.php');
		include (TEMPLATEPATH . '/inc/mma_localizacao.php');

		include (TEMPLATEPATH . '/inc/pop-up-agendamento.php');
	?>
</main>

<?php get_footer();
<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MMA
 */

$categoriaprojeto = 'categoriaprojeto';
// LISTA AS CATEGORIAS DE PRODUTO
$categoriaprojetos = get_terms( $categoriaprojeto, array(
	'orderby'    => 'count',
	'hide_empty' => true,
	'parent'	 => '',
	'order'      => 'DESC',
));

get_header();
?>
<main class="pg pg-projetos">
	<section class="secao-destaque-paginas">
		<h4 class="hidden"><?php echo $configuracao['opt_titulo_projetos'] ?></h4>
		<div class="large-container">
			<article>
				<h1 class="titulo"><?php echo $configuracao['opt_titulo_projetos'] ?></h1>
				<p><?php echo $configuracao['opt_texto_projetos'] ?></p>
			</article>
		</div>
	</section>

	<div class="menu-filtro">
		<div class="large-container">
			<div class="row">
				<div class="col-md-6">
					<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
						<input type="text" name="s" id="search" placeholder="Buscar...">
						<div class="div-input-enviar">
							<input type="submit" value="">
						</div>
					</form>
				</div>
				<div class="col-md-6">
					<div class="filtros">
						<div class="filtro-categoria">
							<span class="filtro-categoria-atual">Todos os serviços</span>
							<ul class="filtro-categoria-opcoes">
								<?php 
									foreach ($categoriaprojetos as $categoriaprojetos):
									$term_id = $categoriaprojetos->term_id;
									$categoriaName = $categoriaprojetos->name;
								?>
								<li class="filtro-categoria-opcao">
									<a href="<?php echo get_term_link($term_id) ?>">
										<p><?php echo $categoriaName ?></p>
									</a>
								</li>
								<?php endforeach; ?>
							</ul>
						</div>
						<span class="modelo">
							<img src="<?php echo get_template_directory_uri(); ?>/img/listactive.svg" alt="Visualizar em lista" class="list modelo-active">
						</span>
						<span class="modelo">
							<img src="<?php echo get_template_directory_uri(); ?>/img/gridactive.svg" alt="Visualizar em tabela" class="grid">
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="secao-projetos">
		<h4 class="hidden">SEÇÃO PROJETOS</h4>
		<div class="large-container">
			<ul class="lista-projetos">
				<?php while ( have_posts() ){ the_post(); 
					include (TEMPLATEPATH . '/inc/template_projeto.php');
				} ?>
			</ul>
		</div>
	</section>
	
<?php  
	include (TEMPLATEPATH . '/inc/mma_store.php');
	include (TEMPLATEPATH . '/inc/acessoria_mma.php');
	include (TEMPLATEPATH . '/inc/mma_localizacao.php');

	include (TEMPLATEPATH . '/inc/pop-up-agendamento.php');
	
?>
</main>


<?php
get_footer();

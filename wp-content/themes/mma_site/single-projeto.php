<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package MMA
 */

get_header();


	while ( have_posts() ) :
		the_post();
		$projeto_galeria_carrossel   = rwmb_meta('MMA_projeto_galeria');
		$projeto_galeria_carrossel_2 = rwmb_meta('MMA_projeto_galeria_2');
		$projeto_galeria_carrossel_3 = rwmb_meta('MMA_projeto_galeria_3');
		$single_projeto_servico = rwmb_meta('MMA_single_projeto_servico');

		$id_video_galeria_1 = rwmb_meta('MMA_id_video_galeria_1');
		$destaque_galeria_1 = rwmb_meta('MMA_destaque_galeria_1');
		$posicao_video_galeria_1 = (int)rwmb_meta('MMA_posicao_video_galeria_1');

		$id_video_galeria_2 = rwmb_meta('MMA_id_video_galeria_2');
		$destaque_galeria_2 = rwmb_meta('MMA_destaque_galeria_2');
		$posicao_video_galeria_2 = (int)rwmb_meta('MMA_posicao_video_galeria_2');

		$id_video_galeria_3 = rwmb_meta('MMA_id_video_galeria_3');
		$destaque_galeria_3 = rwmb_meta('MMA_destaque_galeria_3');
		$posicao_video_galeria_3 = (int)rwmb_meta('MMA_posicao_video_galeria_3');

?>
<main class="pg pg-projeto">
	<section class="secao-destaque-single" style="background-image: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>)">
		<h4 class="hidden"><?php echo get_the_title() ?></h4>
		<figure class="hidden">
			<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>">
			<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
		</figure>
	</section>

	<section class="secao-sobre-projeto">
		<h4 class="hidden"><?php echo get_the_title() ?></h4>
		<div class="mid-container">
			<a href="<?php echo $_SERVER['HTTP_REFERER']; ?>" class="voltar"><img src="<?= get_template_directory_uri(); ?>/img/arrowservicosleftblack.svg" alt="Seta voltar"></a>
			<article>
				<h2 class="titulo"><?php echo get_the_title(); ?></h2>
				<div class="sobre-projeto">
					<h4 class="subtitulo">Sobre o projeto</h4>
					<p><?php echo get_the_content() ; ?></p>
				</div>
				<ul>
					 <?php 
					 	$contador = 0;
						//LOOP DE POST PROJETOS
						$servicos = new WP_Query( array( 'post_type' => 'servico', 'orderby' => 'id', 'posts_per_page' => -1) );
						while ( $servicos->have_posts() ) : $servicos->the_post();

							$terms = get_the_terms( $post->ID, 'categoriaservicos' );
							foreach ($terms as $term) {
								$term_link = get_term_link($term->slug, 'categoriaservicos');
							}

							$verificacao =  in_array($post->ID, $single_projeto_servico);

							if ($verificacao):
					?>
					<li><a href="<?php echo $term_link; ?>" target="_blank"><?php echo get_the_title(); ?></a></li>
					<?php $contador++; endif; endwhile; wp_reset_query(); ?>
				</ul>
			</article>
		</div>
	</section>

	<section class="secao-galeria-projeto">
		<h4 class="hidden"><?php echo get_the_title() ?></h4>
		<div class="large-container">
			<ul class="lista-fotos">
				<?php
					$contador_posicao_galeria_1 = 0;
					$cont2 = 0; 
					$cont = 1;
					$projeto_galeria = rwmb_meta('MMA_projeto_galeria');
					foreach ($projeto_galeria as $projeto_galeria):
						// var_dump($projeto_galeria);

						if($cont == 2){
							$classe = "evento-centro";
						}else if($cont == 5){
							$classe = "large-image large-image-right";
						}else{
							$classe = "evento";
						}

						if(rwmb_meta('MMA_id_video_galeria_1') && rwmb_meta('MMA_destaque_galeria_1') && $contador_posicao_galeria_1 == $posicao_video_galeria_1):
				?>

				<li data-id="<?php echo $cont2; ?>" class="<?php echo $classe; ?>">
					<figure>
						<img src="<?php echo get_template_directory_uri(); ?>/img/1200x800.jpg" data-src="<?php echo rwmb_meta('MMA_destaque_galeria_1')['full_url']; ?>" data-srcset="<?php echo rwmb_meta('MMA_destaque_galeria_1')['full_url']; ?>" alt="imagem video" class="lazy imagem-video">
						<span class="play-button"><img src="<?php echo get_template_directory_uri(); ?>/img/playcircle.svg" alt="Play button"></span>
					</figure>
				</li>

				<?php else: ?>

				<li data-id="<?php echo $cont2; ?>" class="<?php echo $classe; ?>">
					<figure>
						<img class="lazy" src="<?php echo get_template_directory_uri(); ?>/img/1200x800.jpg" data-src="<?php echo $projeto_galeria['full_url'] ?>" data-srcset="<?php echo $projeto_galeria['full_url'] ?>" alt="<?php echo $projeto_galeria['full_url'] ?>">
						<figcaption class="hidden"><?php echo $projeto_galeria['full_url'] ?></figcaption>
					</figure>
				</li>

				<?php endif; $cont2++; $cont++; $contador_posicao_galeria_1++; endforeach; ?>
			</ul>

			<ul class="lista-fotos">
				<?php 
					$contador_posicao_galeria_2 = 0;
					$cont = 1;
					$projeto_galeria_2 = rwmb_meta('MMA_projeto_galeria_2');
					foreach ($projeto_galeria_2 as $projeto_galeria_2):

						if($cont == 2){
							$classe = "evento-centro";
						}else if($cont == 4){
							$classe = "large-image large-image-left";
						}else{
							$classe = "evento";
						}
						
						if(rwmb_meta('MMA_id_video_galeria_2') && rwmb_meta('MMA_destaque_galeria_2') && $contador_posicao_galeria_2 == $posicao_video_galeria_2):
				?>

				<li data-id="<?php echo $cont2; ?>" class="<?php echo $classe; ?>">
					<figure>
						<img src="<?php echo get_template_directory_uri(); ?>/img/1200x800.jpg" data-src="<?php echo rwmb_meta('MMA_destaque_galeria_2')['full_url']; ?>" data-srcset="<?php echo rwmb_meta('MMA_destaque_galeria_2')['full_url']; ?>" alt="imagem video" class="lazy imagem-video">
						<span class="play-button"><img src="<?php echo get_template_directory_uri(); ?>/img/playcircle.svg" alt="Play button"></span>
					</figure>
				</li>

				<?php else: ?>

				<li data-id="<?php echo $cont2; ?>" class="<?php echo $classe; ?>">
					<figure>
						<img class="lazy" src="<?php echo get_template_directory_uri(); ?>/img/1200x800.jpg" data-src="<?php echo $projeto_galeria_2['full_url'] ?>" data-srcset="<?php echo $projeto_galeria_2['full_url'] ?>" alt="<?php echo $projeto_galeria_2['full_url'] ?>">
						<figcaption class="hidden"><?php echo $projeto_galeria_2['full_url'] ?></figcaption>
					</figure>
				</li>
				<?php endif; $cont2++; $cont++; $contador_posicao_galeria_2++; endforeach; ?>
			</ul>

			<ul class="lista-fotos">
				<?php 
					$contador_posicao_galeria_3 = 0;
					$cont = 1;
					$projeto_galeria_3 = rwmb_meta('MMA_projeto_galeria_3');
					foreach ($projeto_galeria_3 as $projeto_galeria_3):

						if($cont == 2){
							$classe = "evento-centro";
						}else if($cont == 5){
							$classe = "large-image large-image-right";
						}else{
							$classe = "evento";
						}

						if(rwmb_meta('MMA_id_video_galeria_3') && rwmb_meta('MMA_destaque_galeria_2') && $contador_posicao_galeria_3 == $posicao_video_galeria_3):
				?>
				<li data-id="<?php echo $cont2; ?>" class="<?php echo $classe; ?>">
					<figure>
						<img src="<?php echo get_template_directory_uri(); ?>/img/1200x800.jpg" data-src="<?php echo rwmb_meta('MMA_destaque_galeria_3')['full_url']; ?>" data-srcset="<?php echo rwmb_meta('MMA_destaque_galeria_3')['full_url']; ?>" alt="imagem video" class="lazy imagem-video">
						<span class="play-button"><img src="<?php echo get_template_directory_uri(); ?>/img/playcircle.svg" alt="Play button"></span>
					</figure>
				</li>

				<?php else: ?>

				<li data-id="<?php echo $cont2; ?>" class="<?php echo $classe; ?>">
					<figure>
						<img class="lazy" src="<?php echo get_template_directory_uri(); ?>/img/1200x800.jpg" data-src="<?php echo $projeto_galeria_3['full_url'] ?>" data-srcset="<?php echo $projeto_galeria_3['full_url'] ?>" alt="<?php echo $projeto_galeria_3['full_url'] ?>">
						<figcaption class="hidden"><?php echo $projeto_galeria_3['full_url'] ?></figcaption>
					</figure>
				</li>
				<?php endif; $cont2++; $cont++; $contador_posicao_galeria_3++; endforeach; ?>
			</ul>
			
		</div>
	</section>

	<!-- SE HOUVER POST POSTERIOR MOSTRA A FOTO SE NÃO MOSTRA A FOTO DO PRIMEIRO POST DISPONÍVEL -->
	<?php 

		$nextPost = get_next_post(); 
		$nextFoto = wp_get_attachment_image_src( get_post_thumbnail_id($nextPost->ID), 'full' );
		$nextFoto = $nextFoto[0];

		$previousPost = get_previous_post();
		$previousPostFoto = wp_get_attachment_image_src( get_post_thumbnail_id($previousPost->ID), 'full' );
		$previousPostFoto = $previousPostFoto[0];
		$previousPost->post_title;
	?>
	<div class="proximos-projetos">
		<ul class="paginador">
			
			<li class="arrow-left">
				<?php if ($nextPost):?>
				<a href="<?php echo $nextPost->guid ?>">
					<span class="seta-paginador">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arrowservicosleftblack.svg" alt="Seta projeto anterior" class="black-arrow">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arrowleftwhite.svg" alt="Seta projeto anterior" class="white-arrow">
					</span>
					<figure>
						<img src="<?php echo $nextFoto ?>" alt="<?php echo $nextPost->post_title ?>">
						<figcaption class="hidden"><?php echo $nextPost->post_title ?></figcaption>
					</figure>
					<div class="nome-projeto">
						<h2 class="titulo"><?php echo $nextPost->post_title ?></h2>
					</div>
				</a>
				<?php endif;  ?>
			</li>
		
			<li class="arrow-right">
				<?php  if ($previousPost): ?>
				<a href="<?php echo $previousPost->guid ?>">
					<div class="nome-projeto">
						<h2 class="titulo"><?php echo $previousPost->post_title ?></h2>
					</div>
					<figure>
						<img src="<?php echo $previousPostFoto ?>" alt="<?php echo $previousPost->post_title ?>">
						<figcaption class="hidden"><?php echo $previousPost->post_title ?></figcaption>
					</figure>
					<span class="seta-paginador">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arrowservicosrightblack.svg" alt="Seta projeto anterior" class="black-arrow">
						<img src="<?php echo get_template_directory_uri(); ?>/img/arrowrightwhite.svg" alt="Seta projeto anterior" class="white-arrow">
					</span>
					<?php endif;?>
				</a>
			</li>
			
		</ul>
	</div>
	
	<?php  
		include (TEMPLATEPATH . '/inc/galeria.php');
		include (TEMPLATEPATH . '/inc/mma_store.php');
		include (TEMPLATEPATH . '/inc/acessoria_mma.php');
		include (TEMPLATEPATH . '/inc/mma_localizacao.php');
		include (TEMPLATEPATH . '/inc/pop-up-agendamento.php');
	?>

	<div class="pop-up-galeria-projeto">
		<span>
			<img src="<?php echo get_template_directory_uri(); ?>/img/close.svg" alt="Fechar pop-up">
		</span>
		<ul class="carrossel-projetos-galeria owl-carousel">
			<?php $contador_posicao_galeria_1 = 0; foreach ($projeto_galeria_carrossel as $projeto_galeria_carrossel): if($destaque_galeria_1 && $id_video_galeria_1 && $contador_posicao_galeria_1 == $posicao_video_galeria_1): ?>
				<li class="item-projetos">
					<figure>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $id_video_galeria_1; ?>?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</figure>
				</li>
				<?php else: ?>
				<li class="item-projetos">
					<figure>
						<img src="<?php echo $projeto_galeria_carrossel['full_url'] ?>" alt="<?php echo $projeto_galeria_carrossel['full_url'] ?>">
						<figcaption><?php echo $projeto_galeria_carrossel['full_url'] ?></figcaption>
					</figure>
				</li>
			<?php endif; $contador_posicao_galeria_1++; endforeach; ?>

			<?php $contador_posicao_galeria_2 = 0; foreach ($projeto_galeria_carrossel_2 as $projeto_galeria_carrossel_2): if($destaque_galeria_2 && $id_video_galeria_2 && $contador_posicao_galeria_2 == $posicao_video_galeria_2): ?>
				<li class="item-projetos">
					<figure>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $id_video_galeria_2; ?>?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</figure>
				</li>
				<?php else: ?>
				<li class="item-projetos">
					<figure>
						<img src="<?php echo $projeto_galeria_carrossel_2['full_url'] ?>" alt="<?php echo $projeto_galeria_carrossel_2['full_url'] ?>">
						<figcaption><?php echo $projeto_galeria_carrossel_2['full_url'] ?></figcaption>
					</figure>
				</li>
			<?php endif; $contador_posicao_galeria_2++; endforeach; ?>

			<?php $contador_posicao_galeria_3 = 0; foreach ($projeto_galeria_carrossel_3 as $projeto_galeria_carrossel_3): if($destaque_galeria_3 && $id_video_galeria_3 && $contador_posicao_galeria_3 == $posicao_video_galeria_3): ?>
				<li class="item-projetos">
					<figure>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $id_video_galeria_3; ?>?enablejsapi=1&version=3&playerapiid=ytplayer" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</figure>
				</li>
				<?php else: ?>
				<li class="item-projetos">
					<figure>
						<img src="<?php echo $projeto_galeria_carrossel_3['full_url'] ?>" alt="<?php echo $projeto_galeria_carrossel_3['full_url'] ?>">
						<figcaption><?php echo $projeto_galeria_carrossel_3['full_url'] ?></figcaption>
					</figure>
				</li>
			<?php endif; $contador_posicao_galeria_3++; endforeach; ?>
		</ul>
	</div>

</main>

<?php endwhile; wp_reset_query(); get_footer();

<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package MMA
 */
global $post;
get_header();

$numero_whtas_remocao_espaco_externo = trim($configuracao['opt_telefone']);
$numero_whtas_remocao_traco = str_replace(" ","",$numero_whtas_remocao_espaco_externo);
$numeroFomratado = str_replace("-","",$numero_whtas_remocao_traco);

$categoria = 'categoriaservicos';
// LISTA AS CATEGORIAS DE PRODUTO
$categoriaServicos = get_terms( $categoria, array(
	'orderby'    => 'count',
	'hide_empty' => true,
	'parent'	 => '',
	'order'      => 'DESC',
));

$categoriaServicosLoop = get_terms( $categoria, array(
	'orderby'    => 'count',
	'hide_empty' => true,
	'parent'	 => '',
	'order'      => 'DESC',
));
// $projeto_ferramentas = rwmb_meta('MMA_servico_projeto'); 
?>

<style type="text/css">
	.pg-servicos section.servico-destaque{
		background-image: url(<?php echo $configuracao['opt_texto_servicos_banner']['url'] ?>)
	}
	@media(max-width: 500px){
		.pg-servicos section.servico-destaque{
			background-image: url(<?php echo $configuracao['opt_texto_servicos_banner_mobile']['url'] ?>)
		}
	}
</style>

<main class="pg pg-servicos">

	<section class="servico-destaque">
		
		<div class="servico-destaque-texto large-container">
			<h3 class="titulo"><?php echo $configuracao['opt_titulo_servicos'] ?></h3>
			<p><?php echo $configuracao['opt_texto_servicos'] ?></p>
		</div>

		<figure>
			<img src="<?php echo $configuracao['opt_texto_servicos_banner']['url'] ?>" alt="<?php echo $configuracao['opt_titulo_servicos'] ?>">
			<figcaption class="hidden"><?php echo $configuracao['opt_titulo_servicos'] ?></figcaption>
		</figure>

		<nav class="servico-destaque-menu large-container">
			<div class="categoria-ativa">
				<a href="#">
					<figure class="categoria-ativa-imagem">
						<img src="<?php echo get_template_directory_uri(); ?>/img/servicos_menu_icon_active5.svg" alt="">
					</figure>
					<h3 class="categoria-ativa-titulo titulo">Funilaria Artesanal</h3>
				</a>
			</div>
			<hr>
			<ul>
				<?php 
					$i = 0; 
					foreach ($categoriaServicos as $categoriaServicos):
						$categoriaServicosName    = $categoriaServicos->name;
						$categoriaServicosTerm_id = $categoriaServicos->term_id;
				?>
				<li data-id="<?php echo $categoriaServicosTerm_id ?>" data-icone="<?php echo z_taxonomy_image_url($categoriaServicosTerm_id); ?>" data-titulo="<?php echo $categoriaServicosName  ?>" class="<?php if($i == 0){echo "ativo";} ?>">
					<a href="<?php echo $term_link ?>">
						<figure>
							<img src="<?php echo z_taxonomy_image_url($categoriaServicosTerm_id); ?>" alt="<?php echo $categoriaServicosName  ?>" alt="">
						</figure>
						<h3><?php echo $categoriaServicosName  ?></h3>
					</a>
				</li>
				<?php $i++; endforeach; ?>
			</ul>
		</nav>

	</section>

	<div id="servico-ancora"></div>

	<?php
		$cont = 0; 
		foreach ($categoriaServicosLoop as $categoriaServicosLoop):
	?>
	<div id="<?php echo $categoriaServicosLoopTerm_id = $categoriaServicosLoop->term_id; ?>" class="areaServicos <?php if($cont == 0){echo "ativo";} ?>">
		
		<?php 
			$servicos = new WP_Query(array(
				'post_type'     => 'servico',
				'posts_per_page'   => -1,
				'tax_query'     => array(
					array(
						'taxonomy' => 'categoriaservicos',
						'field'    => 'slug',
						'terms'    => $categoriaServicosLoop->slug,
						)
					)
				)
			);

			if ($servicos):

				// LOOP DE POST
				$valor = 0;
				while ( $servicos->have_posts() ) : $servicos->the_post();
					$servicosFoto = wp_get_attachment_image_src( get_post_thumbnail_id($servicos->ID), 'full' )[0];
					$servicoFotoMobile = rwmb_meta('MMA_banner_destaque_servicos_mobile')['full_url'];
					$projeto_ferramentas = rwmb_meta('MMA_servico_projeto');
					if($valor % 2 == 0):
					    
		?>
		<section class="servico-lista">
			
			<div class="row">
				
				<div class="col-md-6">
					<div class="servico-lista-foto">
						<figure class="figure-desktop">
							<img src="<?php echo $servicosFoto; ?>" alt="<?php echo get_the_title() ?>">
							<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
						</figure>
						<figure class="figure-mobile">
							<img src="<?php echo $servicoFotoMobile; ?>" alt="<?php echo get_the_title() ?>">
							<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
						</figure>
					</div>
				</div>
				<div class="col-md-6">
					<div class="servico-lista-texto">
						
						<article>
							<h2><?php echo get_the_title() ?></h2>
							<p><?php echo get_the_content() ?></p>
							<ul>
								<?php 
									$servico_list = rwmb_meta('MMA_servico_list');
									foreach ($servico_list  as $servico_list ):
										$servico_list = $servico_list;
										
								 ?>
								<li><p><?php echo $servico_list ?></p></li>
								<?php endforeach; ?>
							</ul>
						</article>
						
						<div class="servico-lista-projetos">
							<h4>Projetos que receberam esse tratamento</h4>
							<ul>
								<?php 
									$contador_servicos = 0;
									//LOOP DE POST PROJETOS
									$projetos = new WP_Query( array( 'post_type' => 'projeto', 'orderby' => 'id', 'posts_per_page' => -1) );
									while ( $projetos->have_posts() ) : $projetos->the_post();
										// var_dump($projetos->ID);
										$verificacao =  in_array((string)$post->ID,$projeto_ferramentas);
										if ($verificacao):
										// var_dump((string)$post->ID);
								?>
								<li>
									<a href="<?php echo get_permalink() ?>">
										<figure>
											<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
											<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
										</figure>
									</a>
								</li>
								<?php endif; endwhile; wp_reset_query(); ?>
							</ul>
						</div>

						<div class="servico-lista-contato">
							<div class="div-button-padrao">
								<span class="button-padrao button-padrao-preto">Agendar horário</span>
							</div>
							<p class="whatsapp">
								<a href="https://api.whatsapp.com/send?phone=+55<?php echo $numeroFomratado; ?>&text=Ol%C3%A1!"><?php echo $configuracao['opt_telefone']; ?></a>
							</p>
						</div>

					</div>
				</div>

			</div>

		</section>
		<?php else: ?>
		<section class="servico-lista">
			
			<div class="row">

				<div class="col-md-6 col-mobile">
					<div class="servico-lista-foto">
						<figure>
							<img src="<?php echo $servicoFotoMobile; ?>" alt="<?php echo get_the_title() ?>">
							<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
						</figure>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="servico-lista-texto">
						
						<article>
							<h2><?php echo get_the_title() ?></h2>
							<p><?php echo get_the_content() ?></p>
							<ul>
								<?php 
									$servico_list = rwmb_meta('MMA_servico_list');
									foreach ($servico_list  as $servico_list ):
										$servico_list = $servico_list;
										
								 ?>
								<li><p><?php echo $servico_list ?></p></li>
								<?php endforeach; ?>
							</ul>
						</article>
						
						<div class="servico-lista-projetos">
							<h4>Projetos que receberam esse tratamento</h4>
							<ul>
								<?php 
									$contador_servicos2 = 0;
									//LOOP DE POST PROJETOS
									$projetos = new WP_Query( array( 'post_type' => 'projeto', 'orderby' => 'id', 'posts_per_page' => -1) );
									while ( $projetos->have_posts() ) : $projetos->the_post();
										// var_dump($projetos->ID);
										$verificacao =  in_array((string)$post->ID,$projeto_ferramentas);
										if ($verificacao):
										// var_dump((string)$post->ID);
								?>
								<li>
									<a href="<?php echo get_permalink() ?>">
										<figure>
											<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
											<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
										</figure>
									</a>
								</li>
								<?php endif; endwhile; wp_reset_query(); ?>
							</ul>
						</div>

						<div class="servico-lista-contato">
							<div class="div-button-padrao">
								<span class="button-padrao button-padrao-preto">Agendar horário</span>
							</div>
							<p class="whatsapp">
									<a href="https://api.whatsapp.com/send?phone=+55<?php echo $numeroFomratado; ?>&text=Ol%C3%A1!"><?php echo $configuracao['opt_telefone']; ?></a>
							</p>
						</div>

					</div>
				</div>

				<div class="col-md-6 col-desktop">
					<div class="servico-lista-foto">
						<figure>
							<img src="<?php echo $servicosFoto; ?>" alt="<?php echo get_the_title() ?>">
							<figcaption class="hidden"><?php echo get_the_title() ?></figcaption>
						</figure>
					</div>
				</div>

			</div>

		</section>
		<?php endif; $valor++; endwhile; endif; wp_reset_query(); ?>

	</div>
	<?php $cont++;endforeach; ?>
	
	<?php  
		include (TEMPLATEPATH . '/inc/mma_store.php');
		include (TEMPLATEPATH . '/inc/acessoria_mma.php');
		include (TEMPLATEPATH . '/inc/mma_localizacao.php');

		include (TEMPLATEPATH . '/inc/pop-up-agendamento.php');
	?>
</main>
<?php
get_footer();

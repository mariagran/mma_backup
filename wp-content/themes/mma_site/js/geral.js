(function() {

	$(document).ready(function(){

		$('.pop-up-galeria-projeto>span').click(function(){
			$('.pop-up-galeria-projeto .carrossel-projetos-galeria .owl-item.active .item-projetos figure iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
		});
		// $('.pop-up-galeria-projeto .owl-nav button.owl-next').click(function(){
		// 	$('.pop-up-galeria-projeto .carrossel-projetos-galeria .owl-item.active .item-projetos figure iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
		// });
		// $('.pop-up-galeria-projeto .owl-nav button.owl-prev').click(function(){
		// 	$('.pop-up-galeria-projeto .carrossel-projetos-galeria .owl-item.active .item-projetos figure iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
		// });
	    
	    $("#menu-item-116 a").attr('target', '_blank');
	    
	    $('.input-telefone').mask('(00) 00000-0000');

		$('.scrollTop a').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
			
		});

		$('.pg-servicos section.servico-destaque nav.servico-destaque-menu ul li').click(function(){
			event.preventDefault();
			$('.pg-servicos section.servico-destaque nav.servico-destaque-menu ul li').removeClass("ativo");
			$('.pg-servicos .areaServicos').removeClass("ativo");
			$(this).addClass("ativo");
			let dataId = $(this).attr('data-id');
			let dataIcone = $(this).attr('data-icone');
			let dataTitulo = $(this).attr('data-titulo');

			$('.pg-servicos section.servico-destaque nav.servico-destaque-menu .categoria-ativa a .categoria-ativa-titulo').text(dataTitulo);
			$('.pg-servicos section.servico-destaque nav.servico-destaque-menu .categoria-ativa a .categoria-ativa-imagem img').attr('src', dataIcone);
			$("#"+dataId).addClass("ativo");

			if(screen.width <= 768){
				setTimeout(function(){
					$('html,body').animate({
						scrollTop: $('#servico-ancora').offset().top
					}, 1000);
					$('.pg-servicos section.servico-destaque nav.servico-destaque-menu ul').toggleClass('show-menu-categorias');
					return false;
				}, 200);
			}
		});

		$('.tax-categoriaservicos .pg-servicos section.servico-destaque nav.servico-destaque-menu ul li').click(function(){
			event.preventDefault();
			let dataLink = $(this).attr('data-link');
			window.history.pushState("object or string", "Title", dataLink);

		});

		$('.carrossel-servicos').owlCarousel({
			items : 3,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: true,
			touchDrag: true,	
			smartSpeed: 450,
			center: false ,
			margin: 15,
			autoWidth: true,
			// responsiveClass:true,			    
			// responsive:{
			// 	320:{
			// 		items:1,
			// 		margin: 15,
			// 	},
			// 	425:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	500:{
			// 		items:2,
			// 		margin: 15,
			// 	},
			// 	768:{
			// 		items:3,
			// 		margin: 30,
			// 	},
			// 	850:{
			// 		items:4,
			// 	},
			// }
		});

		$('.carrossel-projetos-galeria').owlCarousel({
			items : 1,
			dots: true,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,	
			smartSpeed: 450,
			center: false ,
			margin: 0,
			autoWidth: false,
		});

		$('.carrossel-projeto').owlCarousel({
			items : 1,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,	
			smartSpeed: 450,
			// animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			center: false,
			autoHeight:true,
		});
		//Botões de navegação carrossel de projetos página inicial
		$('.pg .secao-sobre-projeto article .sobre-projeto .buttons-carrossel button.owl-next').click(function(){
			$('.pg-inicial .secao-sobre-projeto .carrossel-projeto .owl-nav .owl-next').click();
		});
		$('.pg .secao-sobre-projeto article .sobre-projeto .buttons-carrossel button.owl-prev').click(function(){
			$('.pg-inicial .secao-sobre-projeto .carrossel-projeto .owl-nav .owl-prev').click();
		});

		$('.carrossel-destaque').owlCarousel({
			items : 1,
			dots: false,
			nav: true,
			loop: false,
			lazyLoad: false,
			mouseDrag: false,
			touchDrag: false,	
			smartSpeed: 450,
			center: false
		});

		$('.pg-projetos .menu-filtro .filtros .modelo img.grid').click(function(){
			$('.pg-projetos .secao-projetos .lista-projetos').addClass('grid-projetos');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-4').addClass('col-12');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-12').removeClass('col-4');
			$('.pg-projetos .secao-projetos .grid-projetos li:nth-child(3n+2)').addClass('evento-centro');
			$('.pg-projetos .menu-filtro .filtros .modelo img').removeClass('modelo-active');
			$(this).addClass('modelo-active');
		});

		$('.pg-projetos .menu-filtro .filtros .modelo img.list').click(function(){
			$('.pg-projetos .secao-projetos .lista-projetos').removeClass('grid-projetos');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-12').addClass('col-4');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-4').removeClass('col-12');
			$('.pg-projetos .secao-projetos .grid-projetos li').removeClass('evento-centro');
			$('.pg-projetos .menu-filtro .filtros .modelo img').removeClass('modelo-active');
			$(this).addClass('modelo-active');
		});

		$('.pg .secao-localizacao>ul>li').click(function(e){
			let myThisUl = $(this).children('article').children('ul');
			let myThisFigure = $(this).children('article').children('figure');
			let myThis = $(this);


			if(!$(this).hasClass('localizacao-active')){
				$('.pg .secao-localizacao>ul>li').removeClass('localizacao-active');
				$('.pg .secao-localizacao ul li .localizacao .sobre-localizacao').removeClass('show-sobre');
				$('.pg .secao-localizacao ul li .localizacao figure').removeClass('show-sobre');

				$(this).addClass('localizacao-active');

				setTimeout(function(){
					myThisUl.addClass('show-sobre');
					myThisFigure.addClass('show-sobre');
				}, 200);
			} else{
				myThisUl.removeClass('show-sobre');
				myThisFigure.removeClass('show-sobre');

				setTimeout(function(){
					myThis.removeClass('localizacao-active');
				}, 200);
			}

		});

		$('.pg .secao-acessoria article form .div-input-enviar input').attr('value', '');

		$('.pg-projetos .menu-filtro .filtros .filtro-categoria .filtro-categoria-atual').click(function(){
			$('.pg-projetos .menu-filtro .filtros .filtro-categoria .filtro-categoria-opcoes').toggleClass('show-options');
			$('.pg-projetos .menu-filtro .filtros .filtro-categoria').toggleClass('show-before');
		});

		$('.contatoMenu a').click(function(e){
			e.preventDefault();
			$('.pop-up-agendamento').addClass('show-pop-up');
			$('body').addClass('stop-scroll');
			setTimeout(function(){
				$('.pop-up-agendamento .pop-up-lente').addClass('show-pop-up-lente');
				setTimeout(function(){
					$('.pop-up-agendamento .pop-up-container').addClass('show-pop-up-container');
				}, 400);
			}, 200);
		});

		$('.div-button-padrao span.button-padrao').click(function(){
			$('.pop-up-agendamento').addClass('show-pop-up');
			$('body').addClass('stop-scroll');
			setTimeout(function(){
				$('.pop-up-agendamento .pop-up-lente').addClass('show-pop-up-lente');
				setTimeout(function(){
					$('.pop-up-agendamento .pop-up-container').addClass('show-pop-up-container');
				}, 400);
			}, 200);
		});

		$('.pop-up-agendamento .close-pop-up').click(function(){
			$('.pop-up-agendamento .pop-up-container').removeClass('show-pop-up-container');
			setTimeout(function(){
				$('.pop-up-agendamento .pop-up-lente').removeClass('show-pop-up-lente');
				setTimeout(function(){
					$('.pop-up-agendamento').removeClass('show-pop-up');
					$('body').removeClass('stop-scroll');
				}, 400);
			}, 200);
		});

		$('.pop-up-agendamento .pop-up-lente').click(function(){
			$('.pop-up-agendamento .pop-up-container').removeClass('show-pop-up-container');
			setTimeout(function(){
				$('.pop-up-agendamento .pop-up-lente').removeClass('show-pop-up-lente');
				setTimeout(function(){
					$('.pop-up-agendamento').removeClass('show-pop-up');
					$('body').removeClass('stop-scroll');
				}, 400);
			}, 200);
		});

		function getOwlDots(){
			let listaFotosGaleria = document.querySelectorAll('.pop-up-galeria-projeto .owl-dots button');

			for(let i = 0; i < listaFotosGaleria.length; i++){
				listaFotosGaleria[i].id = i;
			}
		}
		getOwlDots();

		$('.pg-projeto .secao-galeria-projeto .lista-fotos li').click(function(){
			let dataId = $(this).attr('data-id');

			$('.pop-up-galeria-projeto .owl-dots button#' + dataId).click();
			$(".pop-up-galeria-projeto").fadeIn();
			$("body").addClass("pop-up-galeria-projeto-sombra");
		});

		$('.pop-up-galeria-projeto>span').click(function(){
			$(".pop-up-galeria-projeto").fadeOut();
			$("body").removeClass("pop-up-galeria-projeto-sombra");
		});

		$('header .navbar .open-menu-button').click(function(){
			$('header .navbar .navbar-header').addClass('show-navbar-header');
			$('header .logo-branca').addClass('show-logo-branca');
		});

		$('header .navbar .navbar-header .close-navbar-header').click(function(){
			$('header .navbar .navbar-header').removeClass('show-navbar-header');
			$('header .logo-branca').removeClass('show-logo-branca');
		});

		$('header .navbar .navbar-header ul li.scrollTop a').click(function(){
			$('header .navbar .navbar-header').removeClass('show-navbar-header');
			$('header .logo-branca').removeClass('show-logo-branca');
		});

		$('header .navbar .navbar-header .close-sub-menu').click(function(){
			$('header .navbar .navbar-header ul li .sub-menu').removeClass('show-sub-menu');
			$('header .navbar .navbar-header .close-sub-menu').removeClass('show-close-sub-menu');
		});

		if(screen.width <= 991){
			$('header .navbar .navbar-header ul li#menu-item-33>a').click(function(e){
				$('header .navbar .navbar-header ul li .sub-menu').addClass('show-sub-menu');
				setTimeout(function(){
					$('header .navbar .navbar-header .close-sub-menu').addClass('show-close-sub-menu');
				}, 200);
				return false;
			});
		}

		if(screen.width <= 500){
			$('.pg-projetos .secao-projetos .lista-projetos li .col-4').addClass('col-6');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-8').addClass('col-6');

			$('.pg-projetos .secao-projetos .lista-projetos li .col-4').removeClass('col-4');
			$('.pg-projetos .secao-projetos .lista-projetos li .col-8').removeClass('col-8');

			$('.pg-projetos .menu-filtro .filtros .modelo img.grid').click(function(){
				$('.pg-projetos .secao-projetos .lista-projetos').addClass('grid-projetos');
				$('.pg-projetos .secao-projetos .lista-projetos li .col-imagem').addClass('col-12');
				$('.pg-projetos .secao-projetos .lista-projetos li .col-imagem').removeClass('col-6');
				$('.pg-projetos .secao-projetos .grid-projetos li:nth-child(3n+2)').addClass('evento-centro');
				$('.pg-projetos .menu-filtro .filtros .modelo img').removeClass('modelo-active');
				$(this).addClass('modelo-active');
			});

			$('.pg-projetos .menu-filtro .filtros .modelo img.list').click(function(){
				$('.pg-projetos .secao-projetos .lista-projetos').removeClass('grid-projetos');
				$('.pg-projetos .secao-projetos .lista-projetos li .col-imagem').addClass('col-6');
				$('.pg-projetos .secao-projetos .lista-projetos li .col-imagem').removeClass('col-12');
				$('.pg-projetos .secao-projetos .grid-projetos li').removeClass('evento-centro');
				$('.pg-projetos .menu-filtro .filtros .modelo img').removeClass('modelo-active');
				$(this).addClass('modelo-active');
			});
		}

		$('.pg-servicos section.servico-destaque nav.servico-destaque-menu .categoria-ativa a').click(function(){
			$('.pg-servicos section.servico-destaque nav.servico-destaque-menu ul').toggleClass('show-menu-categorias');
			return false;
		});

		$('.pop-up-agendamento .pop-up article .div-button-padrao .button-padrao').click(function(){
			$('.pop-up-agendamento .pop-up .formulario-contato').addClass('show-formulario');
		});

		$('.pop-up-agendamento .pop-up .close-sub-menu').click(function(){
			$('.pop-up-agendamento .pop-up .formulario-contato').removeClass('show-formulario');
		});
		
		/*
		** FUNÇÕES GALERIA EVENTOS
		** início
		*/ 
		let galleryImages = document.querySelectorAll('.pg-evento .secao-conteudo-evento article .gallery .gallery-item .gallery-icon a img');
		let popUp = document.querySelector('.carrossel-galeria-eventos');
		let homeUrl = document.location.origin;

		const dataIdGalleryImages = () => {
			galleryImages.forEach(function(image, dataId){
				image.setAttribute('data-id', dataId);
			});
		}

		const mountCarousel = (image) => {
			let imageSrc = image.getAttribute('src');
			let treatedImageSrc = treatImageSource(imageSrc);

			let imageLi = document.createElement('li');
			let imageFigure = document.createElement('figure');
			let imageImg = document.createElement('img');
			
			imageImg.classList.add('lazy');
			imageImg.setAttribute('src', homeUrl + '/wp-content/themes/mma_site/img/1200x800.jpg');
			imageImg.setAttribute('alt', treatedImageSrc);
			imageImg.setAttribute('data-src', treatedImageSrc);
			imageImg.setAttribute('data-srcset', treatedImageSrc);

			imageFigure.appendChild(imageImg);
			imageLi.appendChild(imageFigure);

			popUp.appendChild(imageLi);

			dataIdGalleryImages();
		}

		function treatImageSource(src){
			let imageSource = src;
			let imageExtension = src.substr((src.length-4));
			imageSource = src.substr(0, (src.length-12));
			imageSource += imageExtension;

			return imageSource;
		}

		function idDots(){
			let carouselDots = document.querySelectorAll('.pg-evento .pop-up-galeria-eventos .carrossel-galeria-eventos .owl-dots .owl-dot');

			carouselDots.forEach(function(dot, id){
				let dotId = 'dot' + id;
				dot.setAttribute('id', dotId);
			});
		}

		function mountPopUp(){
			galleryImages.forEach(mountCarousel);

			setTimeout(function(){
				$('.carrossel-galeria-eventos').owlCarousel({
					items : 1,
					dots: true,
					nav: true,
					loop: false,
					lazyLoad: false,
					mouseDrag: false,
					touchDrag: false,	
					smartSpeed: 450,
					center: false
				});

				idDots();
			}, 1000);
		}
		mountPopUp();

		$('.pg-evento .secao-conteudo-evento article .gallery .gallery-item .gallery-icon a img').click(function(e){
// 			e.preventDefault();
			let thisImageDataId = $(this).attr('data-id');
			$('.pg-evento .pop-up-galeria-eventos .carrossel-galeria-eventos .owl-dots .owl-dot#dot'+thisImageDataId).click();;

			setTimeout(function(){
				$('.pg-evento .pop-up-galeria-eventos').show();
			}, 200);
			
// 			console.log('return false');
			return false;
		});
		$('.pg-evento .pop-up-galeria-eventos span img').click(function(){
			$('.pg-evento .pop-up-galeria-eventos').hide();
		});
		/*
		** FUNÇÕES GALERIA EVENTOS
		** fim
		*/

	});

	// LAZY LOAD
	document.addEventListener("DOMContentLoaded", function() {
		let lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
		console.log(lazyImages);
		let active = false;

		const lazyLoad = function() {
			if (active === false) {
				active = true;

				setTimeout(function() {
					lazyImages.forEach(function(lazyImage) {
						if ((lazyImage.getBoundingClientRect().top <= window.innerHeight && lazyImage.getBoundingClientRect().bottom >= 0) && getComputedStyle(lazyImage).display !== "none") {
							lazyImage.src = lazyImage.dataset.src;
							lazyImage.srcset = lazyImage.dataset.srcset;
							lazyImage.classList.remove("lazy");

							lazyImages = lazyImages.filter(function(image) {
								return image !== lazyImage;
							});

							if (lazyImages.length === 0) {
								document.removeEventListener("scroll", lazyLoad);
								window.removeEventListener("resize", lazyLoad);
								window.removeEventListener("orientationchange", lazyLoad);
							}
						}
					});

					active = false;
				}, 200);
			}
		};

		document.addEventListener("scroll", lazyLoad);
		window.addEventListener("resize", lazyLoad);
		window.addEventListener("orientationchange", lazyLoad);
	}); 
	
	document.addEventListener("DOMContentLoaded", function() {
        $('.pg .secao-sobre-projeto article .sobre-projeto .buttons-carrossel .owl-next').click();
        $('.pg .secao-sobre-projeto article .sobre-projeto .buttons-carrossel .owl-prev').click();
	});

}())
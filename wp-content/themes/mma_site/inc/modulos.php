<?php

	function base_modulos () {

		//TIPOS DE CONTEÚDO
		conteudos_mma();

		//TAXONOMIA
		taxonomia_mma();

		//META BOXES
		metaboxes_mma();

	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/
	
		function conteudos_mma (){
		
			// CUSTOM POST TYPE DESTAQUES
			tipoDestaque();

			// CUSTOM POST TYPE PROJETOS
			tipoProjetos();

			// CUSTOM POST TYPE SERVIÇO
			tipoServico();

			// CUSTOM POST TYPE SERVIÇO
			tipoEndereco();

			// CUSTOM POST TYPE EVENTOS
			tipoEventos();

		}

	/****************************************************
	* CUSTOM POST TYPE
	*****************************************************/

		// CUSTOM POST TYPE DESTAQUES
		function tipoDestaque() {



			$rotulosDestaque = array(

									'name'               => 'Destaque',
									'singular_name'      => 'destaque',
									'menu_name'          => 'Destaques',
									'name_admin_bar'     => 'Destaques',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo destaque',
									'new_item'           => 'Novo destaque',
									'edit_item'          => 'Editar destaque',
									'view_item'          => 'Ver destaque',
									'all_items'          => 'Todos os destaque',
									'search_items'       => 'Buscar destaque',
									'parent_item_colon'  => 'Dos destaque',
									'not_found'          => 'Nenhum destaque cadastrado.',
									'not_found_in_trash' => 'Nenhum destaque na lixeira.'
								);



			$argsDestaque 	= array(

									'labels'             => $rotulosDestaque,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'destaque' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);



			// REGISTRA O TIPO CUSTOMIZADO

			register_post_type('destaque', $argsDestaque);



		}

		// CUSTOM POST TYPE PROJETOS
		function tipoProjetos() {



			$rotulosProjetos = array(

									'name'               => 'Projeto',

									'singular_name'      => 'projeto',

									'menu_name'          => 'Projetos',

									'name_admin_bar'     => 'projetos',

									'add_new'            => 'Adicionar projeto',

									'add_new_item'       => 'Adicionar novo projeto',

									'new_item'           => 'Novo projeto',

									'edit_item'          => 'Editar projeto',

									'view_item'          => 'Ver projeto',

									'all_items'          => 'Todos os projetos',

									'search_items'       => 'Buscar projeto',

									'parent_item_colon'  => 'Dos projetos',

									'not_found'          => 'Nenhum projeto cadastrado.',

									'not_found_in_trash' => 'Nenhum projeto na lixeira.'

								);



			$argsProjetos 	= array(

									'labels'             => $rotulosProjetos,

									'public'             => true,

									'publicly_queryable' => true,

									'show_ui'            => true,

									'show_in_menu'       => true,

									'menu_position'		 => 4,

									'menu_icon'          => 'dashicons-welcome-widgets-menus',

									'query_var'          => true,

									'rewrite'            => array( 'slug' => 'projetos' ),

									'capability_type'    => 'post',

									'has_archive'        => true,

									'hierarchical'       => false,

									'supports'           => array( 'title', 'thumbnail','editor')

								);



			// REGISTRA O TIPO CUSTOMIZADO

			register_post_type('projeto', $argsProjetos);



		}

		// CUSTOM POST TYPE SERVIÇO
		function tipoServico() {



			$rotulosServico = array(

									'name'               => 'Serviços',

									'singular_name'      => 'Serviço',

									'menu_name'          => 'Serviços',

									'name_admin_bar'     => 'Serviços',

									'add_new'            => 'Adicionar novo',

									'add_new_item'       => 'Adicionar novo serviço',

									'new_item'           => 'Novo serviço',

									'edit_item'          => 'Editar serviço',

									'view_item'          => 'Ver serviço',

									'all_items'          => 'Todos os serviços',

									'search_items'       => 'Buscar serviços',

									'parent_item_colon'  => 'Dos serviços',

									'not_found'          => 'Nenhum serviço cadastrado.',

									'not_found_in_trash' => 'Nenhum serviço na lixeira.'

								);



			$argsServico 	= array(

									'labels'             => $rotulosServico,

									'public'             => true,

									'publicly_queryable' => true,

									'show_ui'            => true,

									'show_in_menu'       => true,

									'menu_position'		 => 4,

									'menu_icon'          => 'dashicons-admin-tools',

									'query_var'          => true,

									'rewrite'            => array( 'slug' => 'servicos' ),

									'capability_type'    => 'post',

									'has_archive'        => true,

									'hierarchical'       => false,

									'supports'           => array( 'title', 'thumbnail', 'editor', 'excerpt' )

								);



			// REGISTRA O TIPO CUSTOMIZADO

			register_post_type('servico', $argsServico);



		}

		// CUSTOM POST TYPE SERVIÇO
		function tipoEndereco() {

			$rotulosEndereco = array(

									'name'               => 'Endereços',
									'singular_name'      => 'Endereço',
									'menu_name'          => 'Endereços',
									'name_admin_bar'     => 'Endereços',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo endereço',
									'new_item'           => 'Novo endereço',
									'edit_item'          => 'Editar endereço',
									'view_item'          => 'Ver endereço',
									'all_items'          => 'Todos os endereços',
									'search_items'       => 'Buscar endereços',
									'parent_item_colon'  => 'Dos endereços',
									'not_found'          => 'Nenhum endereço cadastrado',
									'not_found_in_trash' => 'Nenhum endereço na lixeira'

								);

			$argsEndereco 	= array(

									'labels'             => $rotulosEndereco,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-admin-tools',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'enderecos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title')
								);

			// REGISTRA O TIPO CUSTOMIZADO
			register_post_type('endereco', $argsEndereco);

		}

		// CUSTOM POST TYPE EVENTOS
		function tipoEventos() {



			$rotulosEventos = array(

									'name'               => 'Evento',
									'singular_name'      => 'evento',
									'menu_name'          => 'Eventos',
									'name_admin_bar'     => 'Eventos',
									'add_new'            => 'Adicionar novo',
									'add_new_item'       => 'Adicionar novo evento',
									'new_item'           => 'Novo evento',
									'edit_item'          => 'Editar evento',
									'view_item'          => 'Ver evento',
									'all_items'          => 'Todos os evento',
									'search_items'       => 'Buscar evento',
									'parent_item_colon'  => 'Dos evento',
									'not_found'          => 'Nenhum evento cadastrado.',
									'not_found_in_trash' => 'Nenhum evento na lixeira.'
								);



			$argsEventos 	= array(

									'labels'             => $rotulosEventos,
									'public'             => true,
									'publicly_queryable' => true,
									'show_ui'            => true,
									'show_in_menu'       => true,
									'menu_position'		 => 4,
									'menu_icon'          => 'dashicons-megaphone',
									'query_var'          => true,
									'rewrite'            => array( 'slug' => 'eventos' ),
									'capability_type'    => 'post',
									'has_archive'        => true,
									'hierarchical'       => false,
									'supports'           => array( 'title', 'thumbnail','editor')
								);



			// REGISTRA O TIPO CUSTOMIZADO

			register_post_type('evento', $argsEventos);



		}

	/****************************************************
	* META BOXES
	*****************************************************/

		function metaboxes_mma(){
			add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );
		}


			function registraMetaboxes( $metaboxes ){

				// PREFIX
				$prefix = 'MMA_';

				// METABOX  DESTAQUES
				$metaboxes[] = array(
					'id'			=> 'metaboxDestaques',
					'title'			=> 'Detalhes',
					'pages' 		=> array('destaque'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
						array(
							'name'  => 'Imagem destaque home mobile: ',
							'id'    => "{$prefix}banner_destaque_mobile",
							'type'  => 'single_image'
						), 
						
					),

				);

				// METABOX  SERVIÇO
				$metaboxes[] = array(
					'id'			=> 'metaboxServico',
					'title'			=> 'Detalhes',
					'pages' 		=> array('servico'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(
					    
					    array(
							'name'   => 'Título alternativo: ',
							'id'     => "{$prefix}titulo_alternativo",
							'type'   => 'text',
						),
					    
						array(
							'name'   => 'Imagem carrossel da home: ',
							'id'     => "{$prefix}servico_img_home",
							'type'   => 'single_image',
						),

						array(
							'name'  => 'Imagem destaque serviços mobile: ',
							'id'    => "{$prefix}banner_destaque_servicos_mobile",
							'type'  => 'single_image'
						), 

						array(
							'name'   => 'Check List: ',
							'id'     => "{$prefix}servico_list",
							'type'   => 'text',
							'clone'  => true
						), 

						array(
							'name'  => 'Projetos',
							'id'    	 => "{$prefix}servico_projeto",
							'desc'  	 => '',
							'type'  	 => 'post',
							'post_type'  => 'projeto',
							'field_type' => 'select',
							'multiple' => true
						), 
						
					),

				);

				// METABOX  PROJETO
				$metaboxes[] = array(
					'id'			=> 'metaboxProjeto',
					'title'			=> 'Detalhes do projeto',
					'pages' 		=> array('projeto'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(

						array(
							'name'  => 'Serviços: ',
							'id'    	 => "{$prefix}single_projeto_servico",
							'desc'  	 => '',
							'type'  	 => 'post',
							'post_type'  => 'servico',
							'field_type' => 'select',
							'multiple' => true
						),

					),

				);

				// METABOX  ENDEREÇO
				$metaboxes[] = array(
					'id'			=> 'metaboxProjetoGaleria1',
					'title'			=> 'Detalhes da galeria 1',
					'pages' 		=> array('projeto'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(

						array(
							'name'   => 'Galeria 1: ',
							'id'     => "{$prefix}projeto_galeria",
							'type'   => 'image_advanced',
							'max_file_uploads'   => 6,
						),
						array(
							'name'   => 'ID vídeo galeria 1: ',
							'id'     => "{$prefix}id_video_galeria_1",
							'type'   => 'text',
						), 
						array(
							'name'  => 'Imagem destaque vídeo galeria 1: ',
							'id'    => "{$prefix}destaque_galeria_1",
							'type'  => 'single_image'
						),
						array(
							'name'  => 'Posição do vídeo: ',
							'id'    => "{$prefix}posicao_video_galeria_1",
							'options'         => array(
								'0'	=> '1',
								'1' => '2',
								'2'	=> '3',
								'3'	=> '4',
								'4' => '5',
								'5'	=> '6',
							),
							'type'  => 'select',
							'multiple' => false
						),
						
					),

				);

				// METABOX  ENDEREÇO
				$metaboxes[] = array(
					'id'			=> 'metaboxProjetoGaleria2',
					'title'			=> 'Detalhes da galeria 2',
					'pages' 		=> array('projeto'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(

						array(
							'name'   => 'Galeria 2: ',
							'id'     => "{$prefix}projeto_galeria_2",
							'type'   => 'image_advanced',
							'max_file_uploads'   => 6,
						),
						array(
							'name'   => 'ID vídeo galeria 2: ',
							'id'     => "{$prefix}id_video_galeria_2",
							'type'   => 'text',
						), 
						array(
							'name'  => 'Imagem destaque vídeo galeria 2: ',
							'id'    => "{$prefix}destaque_galeria_2",
							'type'  => 'single_image'
						),
						array(
							'name'  => 'Posição do vídeo: ',
							'id'    => "{$prefix}posicao_video_galeria_2",
							'options'         => array(
								'0'	=> '1',
								'1' => '2',
								'2'	=> '3',
								'3'	=> '4',
								'4' => '5',
								'5'	=> '6',
							),
							'type'  => 'select',
							'multiple' => false
						),
						
					),

				);

				// METABOX  ENDEREÇO
				$metaboxes[] = array(
					'id'			=> 'metaboxProjetoGaleria3',
					'title'			=> 'Detalhes da galeria 3',
					'pages' 		=> array('projeto'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(

						array(
							'name'   => 'Galeria 3: ',
							'id'     => "{$prefix}projeto_galeria_3",
							'type'   => 'image_advanced',
							'max_file_uploads'   => 6,
						),
						array(
							'name'   => 'ID vídeo galeria 3: ',
							'id'     => "{$prefix}id_video_galeria_3",
							'type'   => 'text',
						), 
						array(
							'name'  => 'Imagem destaque vídeo galeria 3: ',
							'id'    => "{$prefix}destaque_galeria_3",
							'type'  => 'single_image'
						),
						array(
							'name'  => 'Posição do vídeo: ',
							'id'    => "{$prefix}posicao_video_galeria_3",
							'options'         => array(
								'0'	=> '1',
								'1' => '2',
								'2'	=> '3',
								'3'	=> '4',
								'4' => '5',
								'5'	=> '6',
							),
							'type'  => 'select',
							'multiple' => false
						),
						
					),

				);

				// METABOX  ENDEREÇO
				$metaboxes[] = array(
					'id'			=> 'metaboxEndereco',
					'title'			=> 'Detalhes',
					'pages' 		=> array('endereco'),
					'context' 		=> 'normal',
					'priority' 		=> 'high',
					'autosave' 		=> false,
					'fields' 		=> array(

						array(
							'name'  => 'Link google maps: ',
							'id'    => "{$prefix}endereco_link",
							'type'  => 'text'
						), 
						
						array(
							'name'  => 'Rua: ',
							'id'    => "{$prefix}endereco_rua",
							'type'  => 'text'
						),

						array(
							'name'  => 'Telefone: ',
							'id'    => "{$prefix}endereco_telefone",
							'type'  => 'text'
						),

						array(
							'name'  => 'E-mail: ',
							'id'    => "{$prefix}endereco_email",
							'type'  => 'text'
						),

						array(
							'name'  => 'Mapa: ',
							'id'    => "{$prefix}endereco_mapa",
							'type'  => 'single_image'
						),

						
					),

				);

				return $metaboxes;

			}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
		function taxonomia_mma () {

			taxonomiaServicos();

			taxonomiaProjeto();

		}

		function taxonomiaServicos() {

			$rotulosTaxonomiaServicos = array(
												'name'              => 'Categorias de serviço',
												'singular_name'     => 'Categorias de serviços',
												'search_items'      => 'Buscar categoria do serviço',
												'all_items'         => 'Todas as categorias',
												'parent_item'       => 'Categoria pai',
												'parent_item_colon' => 'Categoria pai:',
												'edit_item'         => 'Editar categoria do serviço',
												'update_item'       => 'Atualizar categoria',
												'add_new_item'      => 'Nova categoria',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias serviços',
											);

			$argsTaxonomiaServivos 		= array(

												'hierarchical'      => true,
												'labels'            => $rotulosTaxonomiaServicos,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-servicos' ),
											);

			register_taxonomy( 'categoriaservicos', array( 'servico' ), $argsTaxonomiaServivos);

		}

		function taxonomiaProjeto() {

			$rotulosTaxonomiaProjeto = array(
												'name'              => 'Categorias de projeto',
												'singular_name'     => 'Categorias de projeto',
												'search_items'      => 'Buscar categoria do projeto',
												'all_items'         => 'Todas as categorias',
												'parent_item'       => 'Categoria pai',
												'parent_item_colon' => 'Categoria pai:',
												'edit_item'         => 'Editar categoria do projeto',
												'update_item'       => 'Atualizar categoria',
												'add_new_item'      => 'Nova categoria',
												'new_item_name'     => 'Nova categoria',
												'menu_name'         => 'Categorias projetos',
											);

			$argsTaxonomiaProjeto 		= array(

												'hierarchical'      => true,
												'labels'            => $rotulosTaxonomiaProjeto,
												'show_ui'           => true,
												'show_admin_column' => true,
												'query_var'         => true,
												'rewrite'           => array( 'slug' => 'categoria-projetos' ),
											);

			register_taxonomy( 'categoriaprojeto', array( 'projeto' ), $argsTaxonomiaProjeto);

		}
  	/****************************************************
	* AÇÕES
	*****************************************************/

		// INICIA A FUNÇÃO PRINCIPAL
		add_action('init', 'base_modulos');

		// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
		//add_action( 'add_meta_boxes', 'metaboxjs');

		// FLUSHS
		function rewrite_flush() {
	    	base_modulos();
	   		flush_rewrite_rules();
		}
		register_activation_hook( __FILE__, 'rewrite_flush' );


		$user_id = get_current_user_id();
		if ($user_id <1) { //em 10, adicione o ID do usuário que deseja remover os itens do menu

			function remove_menus(){

				remove_menu_page( 'index.php' ); //Dashboard 
				remove_menu_page( 'edit.php' ); //Posts - publicações
				remove_menu_page( 'upload.php' );
				remove_menu_page( 'edit.php?post_type=page' ); //Pages - páginas
				remove_menu_page( 'edit-comments.php' ); //Comments - comentários
				remove_menu_page( 'themes.php' ); //Appearance - aparência (recomendo!)
				remove_menu_page( 'plugins.php' ); //Plugins (recomendo!)
				remove_menu_page( 'users.php' ); //Users - usuários 
				remove_menu_page( 'tools.php' ); //Tools - ferramentas (recomendo!)
				remove_menu_page( 'options-general.php' ); //Settings - configurações 
				remove_menu_page( 'admin.php?page=revslider' ); //revolution slider, se estiver instalado

			}

			add_action( 'admin_menu', 'remove_menus' );
		} 

	remove_action('welcome_panel','wp_welcome_panel');
	add_action('welcome_panel','my_welcome_panel');



	function my_welcome_panel(){
	

		echo '<link rel="stylesheet" href="'.get_template_directory_uri().'/inc/css/admin.css">';

	?>
		<div class="welcome-panel-content">
			<div class="wp-template-coluna">
				<div class="template-coluna">
					<figure>
						<img src="<?php echo get_template_directory_uri(); ?>/inc/assets/mma_logo.svg" alt="<?php echo get_bloginfo();?>">
					</figure>
					<h3>Seja Bem vindo ao painel Administrativo  <strong><?php echo get_bloginfo();?></strong></h3>
					<p>Navegue em seu painel:</p>
					
					<div class="menuNav">
						<ul>
							<li>
								<a href="<?php echo home_url('/'); ?>" target="_blank" class="page-title-action">Acessar meu site</a>
								<a href="http://localhost/projetos/mma_site/wp-admin/edit.php?post_type=destaque" class="page-title-action">Editar carrossel destaque</a>
								<a href="http://localhost/projetos/mma_site/wp-admin/edit.php?post_type=projetos" class="page-title-action">Editar Projetos</a>
								<a href="http://localhost/projetos/mma_site/wp-admin/edit.php?post_type=servico" class="page-title-action">Editar Serviços</a>
								<a href="http://localhost/projetos/mma_site/wp-admin/upload.php" class="page-title-action">Mídias</a>
							</li>
							<li>
								<a href="https://www.youtube.com/watch?v=_KweJ0Lcye8&list=PLBmKaywCJB6OxBbDjHyGa-NczT4D-_oQV" target="_blank" class="page-title-action">Playlist Completa</a>
								
							</li>
						</ul>

					</div>
					
				</div>
				<div class="template-coluna">
					<h3>Veja como utilizar painel Administrativo  <strong><?php echo get_bloginfo();?></strong></h3>
					<iframe  src="https://www.youtube.com/embed/_KweJ0Lcye8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	<?php
	}
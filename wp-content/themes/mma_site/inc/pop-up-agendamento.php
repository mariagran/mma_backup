<div class="pop-up-agendamento">
	<div class="pop-up-lente"></div>
	<div class="pop-up-container">
		<div class="pop-up">
			<div class="row">
				<div class="col-md-7">
					<article>
						<span class="close-pop-up"><img src="<?php echo get_template_directory_uri(); ?>/img/close.svg" alt="Fechar pop-up"></span>
						<h2 class="titulo">Agende <br>seu horário</h2>
						<ul class="lista-enderecos">

							<?php while($enderecos->have_posts()): $enderecos->the_post(); ?>
							<li><a href="<?php echo rwmb_meta('MMA_endereco_link') ?>" target="_blank"><span><?php echo get_the_title(); ?></span> <?php echo rwmb_meta('MMA_endereco_rua'); ?></a></li>
							<?php endwhile; wp_reset_query(); ?>
						</ul>
						<ul class="lista-contatos">
							<?php if($configuracao['opt_telefone_pop_up']): ?>
							<li><?php echo $configuracao['opt_telefone_pop_up']; ?></li>
							<?php endif; if($configuracao['opt_email']): ?>
							<li><?php echo $configuracao['opt_email']; ?></li>
							<?php endif; ?>
						</ul>

						<div class="div-button-padrao">
							<span class="button-padrao button-padrao-preto">Enviar E-mail</span>
						</div>
					</article>
				</div>
				<div class="col-md-5">
					<div class="formulario-contato">
						<span class="close-sub-menu show-close-sub-menu"><img src="<?php echo get_template_directory_uri(); ?>/img/arrowservicosrightwhite.svg" alt="Fechar formulário mobile"></span>
						<span class="close-pop-up"><img src="<?php echo get_template_directory_uri(); ?>/img/close.svg" alt="Fechar pop-up"></span>
						<h2 class="titulo">Agende <br>seu horário</h2>
						<?php echo do_shortcode('[contact-form-7 id="248" title="Formulário de contato pop-up"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if (trim($configuracao['opt_agende_titulo_home'])): ?>
	<div class="anuncio agende-seu-horario">
		<div class="large-container">
			<div class="anuncio-background anuncio-background-agendamento">
				<div class="row">
					<div class="col-sm-7">
						<h2 class="titulo"><?php echo $configuracao['opt_agende_titulo_home'] ?></h2>
					</div>
					<div class="col-sm-5">
						<div class="div-button-padrao">
							<span class="button-padrao">Agendar Horário</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>

<style type="text/css">
	.anuncio-background-agendamento{
		background-image: url('<?php echo $configuracao['opt_agende_foto_home']['url'] ?>');
	}

	@media(max-width: 500px){
		.anuncio-background-agendamento{
			background-image: url('<?php echo $configuracao['opt_agende_foto_home_mobile']['url'] ?>');
		}
	}
</style>
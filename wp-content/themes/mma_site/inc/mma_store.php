<?php if (trim($configuracao['opt_agende_titulo_home'])): ?>
<div class="anuncio conheca-store">
	<div class="large-container">
		<div class="anuncio-background anuncio-background-store">
			<div class="row">
				<div class="col-sm-7">
					<h2 class="titulo"><?php echo $configuracao['opt_mma_store_titulo_home'] ?></h2>
				</div>
				<div class="col-sm-5">
					<div class="div-button-padrao">
						<a href="http://www.mmacarcare.com.br/" class="button-padrao" target="_blank">Visitar</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

<style type="text/css">
	.anuncio-background-store{
		background-image: url('<?php echo $configuracao['opt_mma_store_foto_home']['url'] ?>');
	}

	@media(max-width: 500px){
		.anuncio-background-store{
			background-image: url('<?php echo $configuracao['opt_mma_store_foto_home_mobile']['url'] ?>');
		}
	}
</style>